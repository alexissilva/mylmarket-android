package cl.silvap.mylmarket.model;

import cl.silvap.mylmarket.util.realm.Identifiable;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class Race extends RealmObject implements Identifiable {

    @PrimaryKey
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmResults<Card> getCards() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Card> cards = realm.where(Card.class).equalTo("race.id", id).findAll();
        realm.close();
        return cards;
    }

}
