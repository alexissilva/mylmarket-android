package cl.silvap.mylmarket.model;

import cl.silvap.mylmarket.util.realm.Identifiable;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class Card extends RealmObject implements Identifiable {

    @PrimaryKey
    private String id;
    private String name;
    private int type;
    private int cost;
    private String ability;
    private int frequency;
    private String code;
    private String illustrator;
    private String image;
    private Edition edition;
    private Integer force;
    private Race race;
    private RealmList<Mechanic> mechanics;

    //local attribute - calculated on the server
    private Float averagePrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIllustrator() {
        return illustrator;
    }

    public void setIllustrator(String illustrator) {
        this.illustrator = illustrator;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Edition getEdition() {
        return edition;
    }

    public void setEdition(Edition edition) {
        this.edition = edition;
    }

    public Integer getForce() {
        return force;
    }

    public void setForce(Integer force) {
        this.force = force;
    }

    public Race getRace() {
        return race;
    }

    public void setRace(Race race) {
        this.race = race;
    }

    public RealmList<Mechanic> getMechanics() {
        return mechanics;
    }

    public void setMechanics(RealmList<Mechanic> mechanics) {
        this.mechanics = mechanics;
    }

    public Float getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(Float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public RealmResults<Offer> getOffers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Offer> offers = realm.where(Offer.class).equalTo("card.id", id).findAll();
        realm.close();
        return offers;
    }
}
