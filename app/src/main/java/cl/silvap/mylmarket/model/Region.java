package cl.silvap.mylmarket.model;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class Region extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmResults<Locality> getCards() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Locality> localities = realm.where(Locality.class).equalTo("region.id", id).findAll();
        realm.close();
        return localities;
    }
}
