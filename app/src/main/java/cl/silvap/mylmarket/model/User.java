package cl.silvap.mylmarket.model;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class User extends RealmObject {

    @PrimaryKey
    private String id;
    private String username;
    private String email;
    private String password;
    private String phone;
    private boolean sends;
    private Locality locality;
    private RealmList<Card> wishItems;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isSends() {
        return sends;
    }

    public void setSends(boolean sends) {
        this.sends = sends;
    }

    public Locality getLocality() {
        return locality;
    }

    public void setLocality(Locality locality) {
        this.locality = locality;
    }

    public RealmList<Card> getWishItems() {
        return wishItems;
    }

    public void setWishItems(RealmList<Card> wishItems) {
        this.wishItems = wishItems;
    }

    public RealmResults<Offer> getOffers() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Offer> offers = realm.where(Offer.class).equalTo("user.id", id).findAll();
        realm.close();
        return offers;
    }
}
