package cl.silvap.mylmarket.model;

import java.util.Date;

import cl.silvap.mylmarket.util.realm.Identifiable;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class Edition extends RealmObject implements Identifiable{

    @PrimaryKey
    private String id;
    private String name;
    private String logo;
    private Date launchDate;
    private Edition father;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Date getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(Date launchDate) {
        this.launchDate = launchDate;
    }

    public Edition getFather() {
        return father;
    }

    public void setFather(Edition father) {
        this.father = father;
    }

    public RealmResults<Card> getCards() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Card> cards = realm.where(Card.class).equalTo("edition.id", id).findAll();
        realm.close();
        return cards;
    }
}
