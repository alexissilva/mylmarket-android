package cl.silvap.mylmarket.model;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Alexis on 08-01-17.
 */

public class Mechanic extends RealmObject {

    @PrimaryKey
    private String id;
    private String name;
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RealmResults<Card> getCards() {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Card> cards = realm.where(Card.class).equalTo("mechanics.id", id).findAll();
        realm.close();
        return cards;
    }
}
