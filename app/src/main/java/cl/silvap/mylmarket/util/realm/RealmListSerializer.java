package cl.silvap.mylmarket.util.realm;

import com.google.common.base.CaseFormat;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

/**
 * Created by Alexis on 08-07-15.
 */
public class RealmListSerializer<E extends RealmObject> implements JsonSerializer<List<E>>, JsonDeserializer<List<E>> {


    private Type type;
    private CaseFormat namingPolicy;

    public RealmListSerializer(Type type) {
        this.type = type;
        this.namingPolicy = CaseFormat.LOWER_UNDERSCORE;
    }

    public RealmListSerializer(Type type, CaseFormat namingPolicy) {
        this.type = type;
        this.namingPolicy = namingPolicy;
    }

    @Override
    public JsonElement serialize(List<E> src, Type typeOfSrc, JsonSerializationContext context) {

        JsonArray jsonArray = new JsonArray();
        RealmObjectSerializer realmSerializer = new RealmObjectSerializer(namingPolicy);

        for (E ro : src) {

            JsonElement JsonElement = realmSerializer.serialize(ro, type, context);
            jsonArray.add(JsonElement);
        }

        return jsonArray;
    }

    @Override
    public List<E> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        List<E> list = new ArrayList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        RealmObjectSerializer realmSerializer = new RealmObjectSerializer(namingPolicy);

        for (JsonElement jsonElement : jsonArray) {
            E ro = (E) realmSerializer.deserialize(jsonElement, type, context);
            list.add(ro);
        }

        return list;
    }
}
