package cl.silvap.mylmarket.util;

/**
 * Created by Alexis on 10-01-17.
 */

public class Constants {

    public static String BASE_URL = "http://192.168.0.15:8888";

    public static String API_URL = BASE_URL + "/app_dev.php/";
    public static String IMAGES_URL = BASE_URL + "/images";
}
