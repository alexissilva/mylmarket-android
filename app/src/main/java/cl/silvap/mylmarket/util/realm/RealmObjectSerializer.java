package cl.silvap.mylmarket.util.realm;

import android.util.Log;

import com.google.common.base.CaseFormat;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Alexis on 06-07-15.
 * <p>
 * When it serializes "basic attributes" (not RealmObject or RealmList) uses the default serializer,
 * but for serialize RealmObject attributes creates a JsonObject with its id. The same happens with
 * RealmList attributes, but in that case, creates a JsonArray (with all ids).
 * <p>
 * As when it serializes, to deserialize "basic attributes" uses the default deserializer, but for
 * deserialize RealmObject attributes creates a RealmObject setting only the id (its other attributes
 * will have the default value). To deserialize RealmList attributes create a List of RealmObject only
 * with id too.
 * <p>
 * Note: the RealmObject attributes of the objects to serialize/deserialize must implement Identifiable.
 */
public class RealmObjectSerializer implements JsonSerializer<RealmObject>, JsonDeserializer<RealmObject> {

    private static final HashMap<CaseFormat, List<String>> ID_FORMAT_HASH = new HashMap<>();
    private static final String TAG = RealmObjectSerializer.class.getSimpleName();
    private static final CaseFormat DEFAULT_NAMING_POLICY = CaseFormat.LOWER_UNDERSCORE;

    static {
        List<String> underscoreFormat = new ArrayList<>();
        underscoreFormat.add("%s_id");
        underscoreFormat.add("%s_ids");

        List<String> camelcaseFormat = new ArrayList<>();
        camelcaseFormat.add("%sId");
        camelcaseFormat.add("%sIds");

        ID_FORMAT_HASH.put(CaseFormat.LOWER_UNDERSCORE, underscoreFormat);
        ID_FORMAT_HASH.put(CaseFormat.LOWER_CAMEL, camelcaseFormat);
    }

    private CaseFormat namingPolicy;

    public RealmObjectSerializer() {
        this.namingPolicy = DEFAULT_NAMING_POLICY;
    }

    /**
     * It works only with lowerCamel and lower_underscore.
     */
    public RealmObjectSerializer(CaseFormat namingPolicy) {

        if (namingPolicy != CaseFormat.LOWER_CAMEL && namingPolicy != CaseFormat.LOWER_UNDERSCORE) {
            throw new IllegalArgumentException();
        }
        this.namingPolicy = namingPolicy;
    }

    @Override
    public JsonElement serialize(RealmObject src, Type typeOfSrc, JsonSerializationContext context) {

        JsonObject json = new JsonObject();

        try {

            String clsString = typeOfSrc.toString().substring(typeOfSrc.toString().indexOf(" ") + 1);
            Class cls = Class.forName(clsString);

            Field[] fs = cls.getDeclaredFields();
            for (Field f : fs) {

                //avoid static fields
                if (f.toString().contains("static")) {
                    continue;
                }

                try {

                    String nameUpperCamel = f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                    Method methodGet;
                    try {
                        methodGet = cls.getMethod("get" + nameUpperCamel);
                    } catch (NoSuchMethodException e) {
                        //second option... if it's boolean
                        methodGet = cls.getMethod("is" + nameUpperCamel);
                    }
                    Object fieldValue = methodGet.invoke(src);

                    if (fieldValue == null) {
                        continue;
                    }

                    //Change naming policy
                    String fieldName = CaseFormat.LOWER_CAMEL.to(namingPolicy, f.getName());


                    if (fieldValue instanceof RealmObject && !(fieldValue instanceof Identifiable)) {
                        Log.w(TAG, "A RealmObject attribute (" + fieldValue.getClass() + ") doesn't implement the Identifiable interface. It won't be serialized.");
                    }

                    if (fieldValue instanceof RealmList) {

                        JsonArray jsonArray = new JsonArray();
                        RealmList valueList = (RealmList) fieldValue;
                        for (Object item : valueList) {
                            if (item instanceof Identifiable) {
                                //JsonObject jsonObject = new JsonObject();
                                //jsonObject.addProperty(fieldName + "_" + ID, ((Identifiable) item).getId());
                                //jsonArray.add(jsonObject);
                                String id = ((Identifiable) item).getId();
                                JsonPrimitive jsonPrimitive = new JsonPrimitive(id);
                                jsonArray.add(jsonPrimitive);
                            } else {
                                Log.e(TAG, "Error to serialize field " + f.getName() + " of class " + src.getClass());
                                return null;
                            }
                        }

                        //Create a list with object ids in the json...
                        String jsonFieldName = String.format(ID_FORMAT_HASH.get(namingPolicy).get(1), fieldName);
                        json.add(jsonFieldName, jsonArray);

                    } else if (fieldValue instanceof Identifiable) {

                        //Write id of object in the json
                        String jsonFieldName = String.format(ID_FORMAT_HASH.get(namingPolicy).get(0), fieldName);
                        json.addProperty(jsonFieldName, ((Identifiable) fieldValue).getId());

                    } else if (fieldValue instanceof String || fieldValue instanceof Number
                            || fieldValue instanceof Boolean || fieldValue instanceof Character
                            || fieldValue instanceof Date) {

                        //Use default serializer
                        json.add(fieldName, context.serialize(fieldValue));

                    } else {
                        Log.e(TAG, "Error to serialize field " + f.getName() + " of class " + src.getClass());
                        return null;
                    }

                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                    return null;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                    return null;
                }


            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }

        return json;
    }

    @Override
    public RealmObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();

        try {

            String clsString = typeOfT.toString().substring(typeOfT.toString().indexOf(" ") + 1);
            Class cls = Class.forName(clsString);
            Object object = cls.newInstance();

            Field[] fs = cls.getDeclaredFields();
            for (Field f : fs) {

                //Avoid static fields
                if (f.toString().contains("static")) {
                    continue;
                }

                String methodsetString = "set" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                Method methodSet = cls.getMethod(methodsetString, f.getType());


                //Change naming policy
                String jsonFieldName = CaseFormat.LOWER_CAMEL.to(namingPolicy, f.getName());


                JsonElement valueField;
                Object value;

                if (RealmObject.class.isAssignableFrom(f.getType()) && !Identifiable.class.isAssignableFrom(f.getType())) {
                    Log.w(TAG, "A RealmObject attribute (" + f.getType() + ") doesn't implement the Identifiable interface. It won't be serialized.");
                }


                //Get value from json..
                if (Identifiable.class.isAssignableFrom(f.getType())) {

                    jsonFieldName = String.format(ID_FORMAT_HASH.get(namingPolicy).get(0), jsonFieldName);
                    valueField = jsonObject.get(jsonFieldName);

                } else if (f.getType().equals(RealmList.class)) {

                    jsonFieldName = String.format(ID_FORMAT_HASH.get(namingPolicy).get(1), jsonFieldName);
                    valueField = jsonObject.get(jsonFieldName);

                } else {
                    valueField = jsonObject.get(jsonFieldName);
                }

                if (valueField == null || valueField.isJsonNull()) {
                    //Json doesn't have the field. Jump next iteration...
                    continue;
                }


                //Transform json to java
                if (f.getType().equals(RealmList.class)) {

                    //Create list
                    JsonArray jsonArray = valueField.getAsJsonArray();
                    RealmList newList = new RealmList();

                    Type[] types = ((ParameterizedType) f.getGenericType()).getActualTypeArguments();
                    Type type = types[0];
                    String clsString2 = type.toString().substring(type.toString().indexOf(" ") + 1);
                    Class insideClass = Class.forName(clsString2);

                    if (Identifiable.class.isAssignableFrom(cls)) {

                        for (JsonElement jsonElement : jsonArray) {
                            Identifiable insideObject = (Identifiable) insideClass.newInstance();
                            String uuid = jsonElement.getAsString();
                            insideObject.setId(uuid);

                            newList.add((RealmObject) insideObject);
                        }

                    }

                    methodSet.invoke(object, newList);

                } else if (Identifiable.class.isAssignableFrom(f.getType())) {

                    //Create object
                    Class cls2 = f.getType();
                    Identifiable newObject = (Identifiable) cls2.newInstance();
                    String id = valueField.getAsString();
                    newObject.setId(id);

                    methodSet.invoke(object, newObject);


                } else if (String.class.isAssignableFrom(f.getType()) || f.getType().isPrimitive()
                        || Date.class.isAssignableFrom(f.getType()) || Number.class.isAssignableFrom(f.getType())
                        || Boolean.class.isAssignableFrom(f.getType()) || Character.class.isAssignableFrom(f.getType())) {

                    //Use default deserializer
                    value = context.deserialize(valueField, f.getType());
                    methodSet.invoke(object, value);

                } else {
                    Log.e(TAG, "Error to deserialize field " + f.getName() + " of class " + f.getType());
                    return null;
                }


            }

            return (RealmObject) object;

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return null;
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        }


    }
}
