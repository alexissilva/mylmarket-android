package cl.silvap.mylmarket.util.realm;

import android.util.Log;
import android.util.Pair;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;

/**
 * Created by Alexis on 17-12-15.
 */
public class RealmUtils {


    final private static String TAG = RealmUtils.class.getSimpleName();

    public static <E extends RealmObject & Identifiable> void joinBackRealmLists(Realm realm, Class<E> realmCls) {

        List<E> list = new ArrayList<>(realm.where(realmCls).findAll());
        for (E object : list) {
            joinBackRealmLists(realm, realmCls, object);
        }
    }

    /**
     * It finds in the database all realmobjects that are "RealmList attributes" of and have a reference to {@code realmObject}.
     * Later, it adds realmobjects found to {@code realmObject}.
     * <p>
     * "It adds to father the references of its sons"
     */
    public static <E extends RealmObject & Identifiable> void joinBackRealmLists(Realm realm, Class<E> realmCls, E realmObject) {

        String ID = "id";

        try {

            Class cls = realmCls;
            Field[] fs = cls.getDeclaredFields();

            //Every list field.
            for (Field f : fs) {
                if (f.getType().equals(RealmList.class)) {

                    //Get class of son.
                    Type[] types = ((ParameterizedType) f.getGenericType()).getActualTypeArguments();
                    Type type = types[0];
                    String clsString2 = type.toString().substring(type.toString().indexOf(" ") + 1);
                    Class insideClass = Class.forName(clsString2);

                    //Get name of reference to the father.
                    Field[] fsInside = insideClass.getDeclaredFields();
                    String nameFather = "";
                    for (Field f2 : fsInside) {
                        if (f2.getType().equals(cls)) {

                            if (!nameFather.equals("")) {
                                throw new RuntimeException("The son (" + insideClass + ") has more than one reference to father (" + realmCls + ").");
                            }

                            nameFather = f2.getName();
                        }
                    }

                    //TODO case when the relation is many to many.


                    if (nameFather.equals("")) {
                        Log.w("REALM JOINBACK", "The son (" + insideClass + ") doesn't have a reference to the father (" + realmCls + ").");
                        continue;
                    }


                    //Find sons
                    List sons = realm.where(insideClass).equalTo(nameFather + "." + ID, realmObject.getId()).findAll();
                    RealmList auxList = new RealmList();
                    auxList.addAll(sons);

                    //Set son list in the father.
                    String nameUpperCamel = f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                    Method setMethod = cls.getMethod("set" + nameUpperCamel, f.getType());
                    setMethod.invoke(realmObject, auxList);
                }

            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


    }

    public static <E extends RealmObject & Identifiable> List<E> copyToRealmAndJoin(Realm realm, List<E> realmList) {
        return copyToRealmAndJoin(realm, realmList, false);
    }

    public static <E extends RealmObject & Identifiable> List<E> copyToRealmAndJoin(Realm realm, List<E> realmList, boolean atomic) {


        try {
            if (atomic)
                realm.beginTransaction();

            List<E> copiedObjects = new ArrayList<>();
            for (E realmObject : realmList) {
                copiedObjects.add(copyToRealmAndJoin(realm, realmObject));
            }

            if (atomic)
                realm.commitTransaction();

            return copiedObjects;
        } catch (Exception e) {
            if (atomic)
                realm.cancelTransaction();

            e.printStackTrace();
            throw new RealmException(e.getMessage());
        }
    }

    public static <E extends RealmObject & Identifiable> E copyToRealmAndJoin(Realm realm, E realmObject) {
        return copyToRealmAndJoin(realm, realmObject, false);

    }

    /**
     * Before to copy to Realm an object, it gets from database the "realobject attributes".
     * Later, it sets null this attributes (to avoid overwrite objects), copies the object and gets a "realm reference" for the object copied.
     * Finally, it joins them: add the realmobjects gotten (first step) to the "realm reference".
     *
     * @param atomic if it's true, the transaction will be between a begin and commit transaction.
     */
    public static <E extends RealmObject & Identifiable> E copyToRealmAndJoin(Realm realm, E realmObject, boolean atomic) {

        try {
            if (atomic)
                realm.beginTransaction();


            //Get objects and (methods) for join later
            List<Pair<Method, RealmObject>> joinObjectInfo = new ArrayList<>();
            List<Pair<Method, RealmList>> joinListInfo = new ArrayList<>();

            Class cls = realmObject.getClass();
            Field[] fs = cls.getDeclaredFields();
            for (Field f : fs) {

                if (f.toString().contains("static")) {
                    continue;
                }


                //get "get method"
                String nameUpperCamel = f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
                Method getMethod;
                try {
                    getMethod = cls.getMethod("get" + nameUpperCamel);
                } catch (NoSuchMethodException e) {
                    //second option... if it's a boolean
                    getMethod = cls.getMethod("is" + nameUpperCamel);
                }
                Object fieldValue = getMethod.invoke(realmObject);

                if (fieldValue == null) {
                    continue;
                }

                if (fieldValue instanceof RealmList) {

                    RealmList auxList = (RealmList) fieldValue;

                    RealmList realmValueList = new RealmList();
                    //Save id in a hashset improve the time of "contains"...
                    HashSet<String> idRealValueList = new HashSet<>();
                    for (Object item : auxList) {

                        if (item instanceof Identifiable) {
                            Class<? extends RealmObject> clsAux = (Class<? extends RealmObject>) item.getClass();
                            RealmObject realmValue = realm.where(clsAux).equalTo("id", ((Identifiable) item).getId()).findFirst();

                            if (realmValue == null) {
                                throw new RealmException("The " + identifiableToString((Identifiable) item) + " doesn't exist in the database. It isn't possible copy the " + identifiableToString(realmObject) + ".");
                            }

                            realmValueList.add(realmValue);
                            idRealValueList.add(((Identifiable) item).getId());

                        } else {
                            //TODO check whether is enough with a warning log
                            Log.w("REALM COPY-JOIN", "The elements of a RealmList (" + item.getClass() + ") don't implement the Identifiable interface. It isn't possible join them.");
                        }
                    }

                    //TODO remove the next line of the loop?
                    E oldObject = (E) realm.where(realmObject.getClass()).equalTo("id", realmObject.getId()).findFirst();


                    //keep old elements.... if you download only new data, you should keep old elements.
                    if (oldObject != null) {
                        RealmList oldList = (RealmList) getMethod.invoke(oldObject);
                        for (Object oldItem : oldList) {

                            if (oldItem instanceof Identifiable) {
                                //avoid add twice the same element.
                                if (!idRealValueList.contains(((Identifiable) oldItem).getId())) {
                                    realmValueList.add((RealmObject) oldItem);
                                }
                            } else {
                                //TODO check whether is enough with a warning log
                                Log.w("REALM COPY-JOIN", "The elements of a RealmList (" + oldItem.getClass() + ") don't implement the Identifiable interface. It isn't possible join them.");
                            }


                        }
                    }

                    //get "set method" and save it for join later
                    Method setMethod = cls.getMethod("set" + nameUpperCamel, fieldValue.getClass());
                    joinListInfo.add(new Pair<>(setMethod, realmValueList));

                    //set null value to avoid overwrite..
                    setMethod.invoke(realmObject, (Object) null);

                } else if (fieldValue instanceof RealmObject) {
                    if (fieldValue instanceof Identifiable) {
                        Class<? extends RealmObject> clsAux = (Class<? extends RealmObject>) fieldValue.getClass();
                        RealmObject realmValue = realm.where(clsAux).equalTo("id", ((Identifiable) fieldValue).getId()).findFirst();

                        if (realmValue == null) {
                            throw new RealmException("The " + identifiableToString((Identifiable) fieldValue) + " doesn't exist in the database.It isn't possible copy the " + identifiableToString(realmObject) + ".");
                        }

                        //get "set method" and save it for join later
                        Method setMethod = cls.getMethod("set" + nameUpperCamel, fieldValue.getClass());
                        joinObjectInfo.add(new Pair<>(setMethod, realmValue));

                        //set null value to avoid overwrite..
                        setMethod.invoke(realmObject, (Object) null);

                    } else {
                        //TODO check whether is enough with a warning log
                        Log.w("REALM COPY-JOIN", "A RealmObject attribute (" + fieldValue.getClass() + ") doesn't implement the Identifiable interface. It isn't possible join them.");
                    }

                }
            }


            //Copy and join...
            E copiedObject = realm.copyToRealmOrUpdate(realmObject);
            for (Pair<Method, RealmObject> info : joinObjectInfo) {
                info.first.invoke(copiedObject, info.second);
            }
            for (Pair<Method, RealmList> info : joinListInfo) {
                info.first.invoke(copiedObject, info.second);
            }

            if (atomic)
                realm.commitTransaction();

            return copiedObject;

        } catch (Exception e) {
            if (atomic)
                realm.cancelTransaction();

            e.printStackTrace();
            throw new RealmException("Error when trying to copy the " + identifiableToString(realmObject) + ": " + e.getMessage());
        }
    }

    private static String identifiableToString(Identifiable identifiable) {
        return identifiable.getClass().getSimpleName() + "(" + identifiable.getId() + ")";
    }
}
