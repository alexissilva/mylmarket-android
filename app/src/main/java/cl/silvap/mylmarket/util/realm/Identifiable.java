package cl.silvap.mylmarket.util.realm;

/**
 * Created by Alexis on 11-12-15.
 */
public interface Identifiable {

    void setId(String id);

    String getId();
}
