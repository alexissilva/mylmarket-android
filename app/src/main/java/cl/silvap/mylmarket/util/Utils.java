package cl.silvap.mylmarket.util;

import java.text.Normalizer;

import cl.silvap.mylmarket.model.Card;

/**
 * Created by Alexis on 13-01-17.
 */

public class Utils {

    public static String getImageUrl(Card card) {

        //Ex.: Furia Extensión -> FuriaExtension
        String edition = Normalizer
                .normalize(card.getEdition().getName(), Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .replace(" ", "");

        return Constants.IMAGES_URL + "/" + edition + "/Cartas/" + card.getCode() + ".png";
    }
}
