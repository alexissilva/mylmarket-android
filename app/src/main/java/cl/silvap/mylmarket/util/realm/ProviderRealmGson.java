package cl.silvap.mylmarket.util.realm;

import com.google.common.base.CaseFormat;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.realm.RealmObject;

/**
 * Created by Alexis on 06-07-15.
 */
public class ProviderRealmGson {


    /**
     * Available naming policies and default naming policy.
     */
    public static final int NAMING_LOWER_UNDERSCORE = 0;
    public static final int NAMING_LOWER_CAMEL = 1;
    private static final int DEFAULT_NAMING_POLICY = NAMING_LOWER_CAMEL;

    /**
     * Default format of datetime.
     */
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    /**
     * If it's true, it serializes boolean to integer, i.e., true to 1 and false to 0.
     */
    private static final boolean SERIALIZE_BOOLEAN_TO_INT = false;


    /**
     * @return a configured to serialize realmObjects gson.
     */
    public static Gson provideGson() {
        return provideGson(DEFAULT_DATE_FORMAT, DEFAULT_NAMING_POLICY);
    }

    /**
     * @param dateFormat   used to serialized date.
     * @param namingPolicy used in the json.
     * @return a configured to serialize realmObjects gson.
     */
    public static Gson provideGson(String dateFormat, int namingPolicy) {

        GsonBuilder builder = new GsonBuilder()
                .setDateFormat(dateFormat)
                .setFieldNamingPolicy(getFieldNamingPolicy(namingPolicy))
                .registerTypeHierarchyAdapter(RealmObject.class, new RealmObjectSerializer(getCaseFormat(namingPolicy)))


                // To serialize/deserialize a RealmObject list you must use the functions toJson(String json, Type typeOfT)
                // and fromJson(String json, Type typeOfT), respectively. Also, you must register an adapter per every type
                // of list using the RealmListSerializer class. For example, to serialize/deserialize lists of Object1 and
                // lists of Object2 you must add:
                //      builder
                //          .registerTypeAdapter(new TypeToken<List<Object1>>() {}.getType(),
                //              new RealmListSerializer<Object1>(Object1.class, getCaseFormat(namingPolicy)))
                //
                //          .registerTypeAdapter(new TypeToken<List<Object2>>() {}.getType(),
                //              new RealmListSerializer<Object2>(Object2.class, getCaseFormat(namingPolicy)))
                //
                // Then, to serialize a list of Object1 to call to:
                //      gson.toJson(object1List, new TypeToken<List<Object1>>() {}.getType());
                //
                // And to deserialize a list of Object2 to call to:
                //      gson.fromJson(object2List, new TypeToken<List<Object2>>() {}.getType());


                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .setPrettyPrinting();


        if (SERIALIZE_BOOLEAN_TO_INT) {
            BooleanToIntegerSerializer s = new BooleanToIntegerSerializer();
            builder.registerTypeAdapter(boolean.class, s);
            builder.registerTypeAdapter(Boolean.class, s);
        }

        return builder.create();
    }

    private static FieldNamingPolicy getFieldNamingPolicy(int namingPolicy) {
        switch (namingPolicy) {
            case NAMING_LOWER_UNDERSCORE:
                return FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
            case NAMING_LOWER_CAMEL:
                return FieldNamingPolicy.IDENTITY;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static CaseFormat getCaseFormat(int namingPolicy) {
        switch (namingPolicy) {
            case NAMING_LOWER_UNDERSCORE:
                return CaseFormat.LOWER_UNDERSCORE;
            case NAMING_LOWER_CAMEL:
                return CaseFormat.LOWER_CAMEL;
            default:
                throw new IllegalArgumentException();
        }
    }

}
