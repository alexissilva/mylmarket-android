package cl.silvap.mylmarket.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.silvap.mylmarket.R;
import cl.silvap.mylmarket.model.Card;
import cl.silvap.mylmarket.network.GetAvgPricesTask;
import io.realm.Realm;

/**
 * Created by Alexis on 12-01-17.
 */

public class HomeFragment extends Fragment {

    @BindView(R.id.cardGrid) GridView cardGrid;

    private CardGalleryAdapter cardAdapter;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        Realm realm = Realm.getDefaultInstance();
        List<Card> cards = realm.where(Card.class).findAll();

        new UpdateAvgPricesTask(cards).execute();

        cardAdapter = new CardGalleryAdapter(getActivity(), cards);
        cardGrid.setAdapter(cardAdapter);

        return view;
    }

    private class UpdateAvgPricesTask extends GetAvgPricesTask {

        public UpdateAvgPricesTask(List<Card> cards) {
            super(cards);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            cardAdapter.notifyDataSetChanged();
        }
    }


}
