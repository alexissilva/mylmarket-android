package cl.silvap.mylmarket.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.silvap.mylmarket.R;
import cl.silvap.mylmarket.model.Card;
import cl.silvap.mylmarket.util.Utils;

/**
 * Created by Alexis on 12-01-17.
 */

public class CardGalleryAdapter extends ArrayAdapter<Card> {

    private LayoutInflater inflater;

    public CardGalleryAdapter(Context context, List<Card> cards) {
        super(context, R.layout.item_card_gallery, cards);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    static class ViewHolder {
        @BindView(R.id.cardImage) ImageView cardImage;
        @BindView(R.id.nameText) TextView nameText;
        @BindView(R.id.priceText) TextView priceText;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    @NonNull
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = inflater.inflate(R.layout.item_card_gallery, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        Card card = getItem(position);
        holder.nameText.setText(card.getName());

        String priceStr;
        if (card.getAveragePrice() != null) {
            Locale l = Locale.getDefault();
            NumberFormat nf = NumberFormat.getCurrencyInstance();
            priceStr = nf.format(card.getAveragePrice());
        } else {
            priceStr = getContext().getString(R.string.cardGalleryAdapter_noPrice);
        }
        holder.priceText.setText(priceStr);

        String imageUrl = Utils.getImageUrl(card);
        Picasso.with(getContext()).load(imageUrl).into(holder.cardImage);

        return view;
    }
}
