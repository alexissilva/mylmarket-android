package cl.silvap.mylmarket.network;

import android.os.AsyncTask;

import java.io.IOException;

import cl.silvap.mylmarket.util.Constants;
import cl.silvap.mylmarket.util.realm.ProviderRealmGson;
import cl.silvap.mylmarket.util.realm.RealmUtils;
import io.realm.Realm;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexis on 10-01-17.
 */

public class DownloadDataTask extends AsyncTask<Void, Void, Void> {

    @Override
    protected Void doInBackground(Void... params) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(ProviderRealmGson.provideGson()))
                .build();

        MainRest mainRest = retrofit.create(MainRest.class);
        Call<DataResponse> call = mainRest.getData();

        Realm realm = Realm.getDefaultInstance();
        try {
            DataResponse response = call.execute().body();


            realm.beginTransaction();
            realm.deleteAll(); //refresh...
            realm.copyToRealmOrUpdate(response.getEditions());
            realm.copyToRealmOrUpdate(response.getRaces());
            RealmUtils.copyToRealmAndJoin(realm, response.getCards());
            realm.commitTransaction();

//            RealmUtils.joinBackRealmLists(realm, Race.class);
//            RealmUtils.joinBackRealmLists(realm, Edition.class);
//            List<Card> cards = realm.where(Card.class).findAll();
//            int number = cards.size();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            realm.close();
        }

        return null;
    }
}
