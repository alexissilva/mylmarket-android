package cl.silvap.mylmarket.network;

import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cl.silvap.mylmarket.model.Card;
import cl.silvap.mylmarket.util.Constants;
import io.realm.Realm;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexis on 14-01-17.
 */

public class GetAvgPricesTask extends AsyncTask<Void, Void, Void> {

    private List<String> cardIds;

    public GetAvgPricesTask(List<Card> cards) {
        cardIds = new ArrayList<>();
        for (Card c : cards) {
            cardIds.add(c.getId());
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        MainRest mainRest = retrofit.create(MainRest.class);
        Call<List<AvgPriceResponse>> call = mainRest.getAveragePrices(cardIds);

        Realm realm = Realm.getDefaultInstance();
        try {
            List<AvgPriceResponse> response = call.execute().body();

            realm.beginTransaction();
            for (AvgPriceResponse priceResponse : response) {
                Card card = realm.where(Card.class).equalTo("id", priceResponse.getCardId()).findFirst();
                card.setAveragePrice(priceResponse.getAveragePrice());
            }
            realm.commitTransaction();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            realm.close();
        }

        return null;
    }

}
