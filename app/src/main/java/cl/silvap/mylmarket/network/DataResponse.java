package cl.silvap.mylmarket.network;

import java.util.List;

import cl.silvap.mylmarket.model.Card;
import cl.silvap.mylmarket.model.Edition;
import cl.silvap.mylmarket.model.Race;

/**
 * Created by Alexis on 10-01-17.
 */

public class DataResponse {

    private List<Card> cards;
    private List<Edition> editions;
    private List<Race> races;

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public List<Edition> getEditions() {
        return editions;
    }

    public void setEditions(List<Edition> editions) {
        this.editions = editions;
    }

    public List<Race> getRaces() {
        return races;
    }

    public void setRaces(List<Race> races) {
        this.races = races;
    }
}
