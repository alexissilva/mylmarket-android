package cl.silvap.mylmarket.network;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Alexis on 10-01-17.
 */

public interface MainRest {

    @GET("data")
    Call<DataResponse> getData();

    @GET("average-prices")
    Call<List<AvgPriceResponse>> getAveragePrices(
            @Query("card_ids[]") List<String> cardIds
    );
}
