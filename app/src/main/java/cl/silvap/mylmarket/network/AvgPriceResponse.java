package cl.silvap.mylmarket.network;

/**
 * Created by Alexis on 14-01-17.
 */

public class AvgPriceResponse {

    private String cardId;
    private Float averagePrice;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Float getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(Float averagePrice) {
        this.averagePrice = averagePrice;
    }


}
